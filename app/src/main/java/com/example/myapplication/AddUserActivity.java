package com.example.myapplication;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.example.myapplication.adapter.CityAdapter;
import com.example.myapplication.adapter.LanguageAdapter;
import com.example.myapplication.database.TblMst_User;
import com.example.myapplication.database.Tbl_City;
import com.example.myapplication.database.Tbl_Language;
import com.example.myapplication.model.CityModel;
import com.example.myapplication.model.LanguageModel;
import com.example.myapplication.model.UserModel;
import com.example.myapplication.util.Constant;
import com.example.myapplication.util.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddUserActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etFatherName)
    EditText etFatherName;
    @BindView(R.id.etsurName)
    EditText etsurName;
    @BindView(R.id.rbMale)
    RadioButton rbMale;
    @BindView(R.id.rbFeMale)
    RadioButton rbFeMale;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.etdob)
    EditText etdob;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.ethobbie)
    EditText ethobbie;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.spLanguage)
    Spinner spLanguage;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    String startingDate = "1990-08-10";

    CityAdapter cityAdapter;
    LanguageAdapter languageAdapter;

    ArrayList<CityModel> cityList = new ArrayList<>();
    ArrayList<LanguageModel> languageList = new ArrayList<>();

    @BindView(R.id.screen_Layout)
    FrameLayout screenLayout;


    UserModel userModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.Text_Add_User), true);
        setDataToView();
        getDataForUpdate();
    }
    void getDataForUpdate() {
        if (getIntent().hasExtra(Constant.USER_OBJECT)) {
            userModel = (UserModel) getIntent().getSerializableExtra(Constant.USER_OBJECT);
            getSupportActionBar().setTitle(R.string.Edit_User);
            etName.setText(userModel.getName());
            etFatherName.setText(userModel.getFatherName());
            etsurName.setText(userModel.getSurName());
            ethobbie.setText(userModel.getHobbies());
            etPhoneNumber.setText(userModel.getPhone_Number());
            etdob.setText(userModel.getDob());
            if (userModel.getGender() == Constant.Male) {
                rbMale.setChecked(true);
            } else {
                rbFeMale.setChecked(true);
            }
            spCity.setSelection(getSelectedPositionFromCityId(userModel.getCity_Id()));
            spLanguage.setSelection(getSelectedPositionFromLanguageId(userModel.getLanguage_Id()));
        }
    }
    void setSpinnerAdapter() {
        cityList.addAll(new Tbl_City(this).getCityList());
        languageList.addAll(new Tbl_Language(this).getLanguages());

        cityAdapter = new CityAdapter(this, cityList);
        languageAdapter = new LanguageAdapter(this, languageList);

        spCity.setAdapter(cityAdapter);
        spLanguage.setAdapter(languageAdapter);

    }

    protected void replaceFragment(@IdRes int containerView, @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getSupportFragmentManager()
                .beginTransaction().add(containerView, fragment, fragmentTag)
                .disallowAddToBackStack().commit();
    }

    void setDataToView() {
        final Calendar newCalendar = Calendar.getInstance();
        etdob.setText(
                String.format("%02d", newCalendar.get(Calendar.DAY_OF_MONTH)) + "/" +
                        String.format("%02d", newCalendar.get(Calendar.MONTH) + 1) + "/" +
                        String.format("%02d", newCalendar.get(Calendar.YEAR)));
        setSpinnerAdapter();
    }

    int getSelectedPositionFromCityId(int cityId) {
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCity_Id() == cityId) {
                return i;
            }
        }
        return 0;
    }

    int getSelectedPositionFromLanguageId(int languageId) {
        for (int i = 0; i < languageList.size(); i++) {
            if (languageList.get(i).getLanguage_Id() == languageId) {
                return i;
            }
        }
        return 0;
    }

    @OnClick(R.id.etdob)
    public void onEtdobClicked() {
        final Calendar newCalendar = Calendar.getInstance();
        Date date = Utils.getDateFromString(startingDate);
        newCalendar.setTimeInMillis(date.getTime());
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                AddUserActivity.this, (datePicker, year, month, day) -> etdob.setText(String.format("%02d", day) + "/" + String.format("%02d", (month + 1)) + "/" + year),
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @OnClick(R.id.btnSubmit)
    public void onBtnSubmitClicked() {

        if (isValidUser()) {
            if (userModel == null) {
                long lastInsetedID = new TblMst_User(getApplicationContext()).insertUser(etName.getText().toString(),
                        etFatherName.getText().toString(), etsurName.getText().toString(), rbMale.isChecked() ? Constant.Male : Constant.Female,
                        ethobbie.getText().toString(),
                        Utils.getFormatedDateToInsert(etdob.getText().toString()),
                        etPhoneNumber.getText().toString(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguage_Id(),
                        cityList.get(spCity.getSelectedItemPosition()).getCity_Id(),1
                        );
                showToast(lastInsetedID > 0 ? "User Inserted Successfully" : "Something went wrong");
            }
            else
            {
                long lastUpdatedID = new TblMst_User(getApplicationContext()).updateUserbyid(userModel.getUserId(),etName.getText().toString(),
                        etFatherName.getText().toString(), etsurName.getText().toString(), rbMale.isChecked() ? Constant.Male : Constant.Female,
                        ethobbie.getText().toString(),
                        Utils.getFormatedDateToInsert(etdob.getText().toString()),
                        etPhoneNumber.getText().toString(),
                        languageList.get(spLanguage.getSelectedItemPosition()).getLanguage_Id(),
                        cityList.get(spCity.getSelectedItemPosition()).getCity_Id(), userModel.getIsFavorite());
                showToast(lastUpdatedID > 0 ? "User Updated Successfully" : "Something went wrong");
            }

        }
    }

    boolean isValidUser() {
        boolean isValid = true;
        if (TextUtils.isEmpty(etName.getText().toString())) {
            isValid = false;
            etName.setError(getString(R.string.lbl_name));
        }

        if (TextUtils.isEmpty(etFatherName.getText().toString())) {
            isValid = false;
            etFatherName.setError(getString(R.string.error_father_name));
        }

        if (TextUtils.isEmpty(etsurName.getText().toString())) {
            isValid = false;
            etsurName.setError(getString(R.string.error_enter_surname));
        }

        if (TextUtils.isEmpty(etPhoneNumber.getText().toString())) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_enter_phone));
        } else if (etPhoneNumber.getText().toString().length() < 10) {
            isValid = false;
            etPhoneNumber.setError(getString(R.string.error_valid_phone));
        }

        if (TextUtils.isEmpty(ethobbie.getText().toString())) {
            isValid = false;
            ethobbie.setError(getString(R.string.error_Enter_Hobbie));
        }

        if (spCity.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_city_select));
        }

        if (spLanguage.getSelectedItemPosition() == 0) {
            isValid = false;
            showToast(getString(R.string.error_Language_Select));
        }
        return isValid;
    }

}
