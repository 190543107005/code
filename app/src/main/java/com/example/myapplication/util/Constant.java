package com.example.myapplication.util;

public class Constant {
    public static final  int Male=1;
    public static final  int Female=2;
    public static final  int Other=3;

    public static final String GENDER = "Gender";

    public static final String USER_OBJECT = "UserObject";
    public static final String USER_ID = "userId";

    public static final String FAVOURITE_CHANGE= "com.example.myapplication.favouritechange";

}
