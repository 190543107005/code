package com.example.myapplication.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.AddUserActivity;
import com.example.myapplication.R;
import com.example.myapplication.adapter.UserListAdapter;
import com.example.myapplication.database.TblMst_User;
import com.example.myapplication.model.UserModel;
import com.example.myapplication.util.Constant;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FragmentUserList extends Fragment {
    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;
    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;
    @BindView(R.id.tvNoDataFound)
    TextView tvNoDataFound;
    static int gender1 = 0;
    @BindView(R.id.btnAddUser)
    FloatingActionButton btnAddUser;
    MyFavouriteChange myFavouriteChange= new MyFavouriteChange();

    public static FragmentUserList getInstance(int gender) {
        gender1 = gender;
        FragmentUserList fragment = new FragmentUserList();
        Bundle bundle = new Bundle();
        bundle.putInt(Constant.GENDER, gender);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user_list, null);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, v);
        setAdapter();
        return v;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.add_user, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            openAddUserScreen();
        }
        return super.onOptionsItemSelected(item);
    }

    void openAddUserScreen() {
        Intent intent = new Intent(getActivity(), AddUserActivity.class);
        startActivity(intent);
    }

    void showAlertDialog(int position) {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure want to delete");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int deleteId = new TblMst_User(getActivity()).deleteUserById(userList.get(position).getUserId());
                        if (deleteId > 0) {
                            Toast.makeText(getActivity(), "Deleted successfully", Toast.LENGTH_SHORT).show();

                            userList.remove(position);
                            adapter.notifyItemRemoved(position);
                            adapter.notifyItemRangeChanged(0, userList.size());
                            checkAndVisibleView();
                        } else {
                            Toast.makeText(getActivity(), "something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    void setAdapter() {
        rcvUserList.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        userList.addAll(new TblMst_User(getActivity()).getUserListByGender(getArguments().getInt(Constant.GENDER)));
        adapter = new UserListAdapter(getActivity(), userList, new UserListAdapter.OnDeleteClickListner() {
            @Override
            public void OnClick(int position) {
                showAlertDialog(position);
            }

            @Override
            public void OnItemView(int position) {
                Intent intent = new Intent(getActivity(), AddUserActivity.class);
                intent.putExtra(Constant.USER_OBJECT, userList.get(position));
                startActivity(intent);

            }

            @Override
            public void onFavouriteClick(int position) {
             int lastUpdatedId=new TblMst_User(getActivity()).updateFavoriteStatus(userList.get(position).getIsFavorite()== 0? 1 : 0 ,userList.get(position).getUserId());
             if(lastUpdatedId>0){
                 userList.get(position).setIsFavorite(userList.get(position).getIsFavorite() == 0? 1: 0);
                adapter.notifyItemChanged(position);
             }
            }

    });
        rcvUserList.setAdapter(adapter);
        checkAndVisibleView();
    }

    void checkAndVisibleView() {
        if (userList.size() > 0) {
            tvNoDataFound.setVisibility(View.GONE);
            rcvUserList.setVisibility(View.VISIBLE);
        } else {
            tvNoDataFound.setVisibility(View.VISIBLE);
            rcvUserList.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btnAddUser)
    public void onViewClicked() {
        openAddUserScreen();
    }
    class MyFavouriteChange extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {

        }
    }
}