package com.example.myapplication;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.adapter.ClgListAdapter;
import com.example.myapplication.api.ApiClient;
import com.example.myapplication.api.ApiInterface;
import com.example.myapplication.model.CollageList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityApiCall extends BaseActivity {


    private static final String NAME = "middle";

    ArrayList<CollageList> lists = new ArrayList<>();
    ArrayList<CollageList> tempLists = new ArrayList<>();
    @BindView(R.id.rcvClgList)
    RecyclerView rcvClgList;

    ClgListAdapter adapter;
    @BindView(R.id.etCountrySearch)
    EditText etCountrySearch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api_call);
        ButterKnife.bind(this);
        getMovieListFromServer();
        setCollagesAdapter();
      searchClgFromServer();
    }
    void resetAdapter(){
        if (adapter != null){
           adapter.notifyDataSetChanged();
        }
    }
void searchClgFromServer(){
        etCountrySearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                tempLists.clear();
                if (charSequence.toString().length() > 0){
                    Log.d("1","execute");
                     for (int i = 0 ;i<lists.size();i++){
                     if (lists.get(i).getCountry().toLowerCase().contains(charSequence.toString().toLowerCase())){
                         tempLists.add(lists.get(i));
                     }
                 }
                }
                if(tempLists.toString().length() == 0 && tempLists.size() == 0){
                    tempLists.addAll(lists);
                }
                resetAdapter();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
}
    void setCollagesAdapter() {
        if (adapter == null) {
            adapter = new ClgListAdapter(this, lists);
            tempLists.addAll(lists);
            rcvClgList.setLayoutManager(new GridLayoutManager(this, 1));
            rcvClgList.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    void getMovieListFromServer() {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getTopRatedMovies(NAME);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    parseApiData(response.body().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void parseApiData(String jsonResponse) {
        try {
            JSONArray array = new JSONArray(jsonResponse);
            for (int i = 0; i < array.length(); i++) {
                JSONObject collageObject = array.getJSONObject(i);
                CollageList model = new CollageList();
                model.setName(collageObject.getString("name"));
                JSONArray urlArray = collageObject.getJSONArray("domains");
                ArrayList<String> domain = new ArrayList<>();
                for (int j = 0; j < urlArray.length(); j++) {
                    domain.add(urlArray.getString(j));
                }
                model.setDomains(domain);
                model.setAlpha_two_code(collageObject.getString("alpha_two_code"));
                model.setCountry(collageObject.getString("country"));
                JSONArray urlArray1 = collageObject.getJSONArray("web_pages");
                ArrayList<String> web_pageList = new ArrayList<>();
                for (int k = 0; k < urlArray1.length(); k++) {
                    web_pageList.add(urlArray1.getString(k));
                }
                model.setWeb_pages(web_pageList);
                lists.add(model);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
