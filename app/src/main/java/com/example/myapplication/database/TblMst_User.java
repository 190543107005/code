package com.example.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.myapplication.model.UserModel;
import com.example.myapplication.util.Utils;

import java.util.ArrayList;

public class TblMst_User extends MyDatabase {
    public static final String TABLE_NAME="TblMst_User";
    public static final String USER_ID="UserId";
    public static final String NAME="Name";
    public static final String FATHER_NAME="FatherName";
    public static final String SURNAME="SurName";
    public static final String GENDER="Gender";
    public static final String HOBBIES="Hobbies";
    public static final String DOB="Dob";
    public static final String PHONE_NUMBER="Phone_Number";
    public static final String LANGUAGE_ID="Language_Id";
    public static final String CITY_ID="City_Id";

    public static final String CITY = "City";
    public static final String LANGUAGE = "Language";

    public static final String IS_FAVOURITE="IsFavourite";
    public TblMst_User(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                "SELECT " +
                        " UserId," +
                        " TblMst_User.Name as Name," +
                        " FatherName," +
                        " SurName," +
                        " Gender," +
                        " Hobbies," +
                        " Dob," +
                        " Phone_Number," +
                        " IsFavourite," +
                        " Tbl_Language.Language_Id," +
                        " Tbl_City.City_Id," +
                        " Tbl_Language.Language_Name as Language," +
                        " Tbl_City.CityName as City " +

                        "FROM " +
                        " TblMst_User " +
                        " INNER JOIN Tbl_Language ON TblMst_User.Language_Id = Tbl_Language.Language_Id" +
                        " INNER JOIN Tbl_City ON TblMst_User.City_Id = Tbl_City.City_Id";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
    public UserModel getCreatedModelUsingCursor(Cursor cursor) {
        UserModel userModel = new UserModel();
        userModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
        userModel.setCity_Id(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        userModel.setLanguage_Id(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        userModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        userModel.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
        userModel.setSurName(cursor.getString(cursor.getColumnIndex(SURNAME)));
        userModel.setGender(cursor.getInt(cursor.getColumnIndex(GENDER)));
        userModel.setHobbies(cursor.getString(cursor.getColumnIndex(HOBBIES)));
        userModel.setDob(Utils.getDateToDisplay(cursor.getString(cursor.getColumnIndex(DOB))));
        userModel.setPhone_Number(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        userModel.setCity(cursor.getString(cursor.getColumnIndex(CITY)));
        userModel.setLanguage(cursor.getString(cursor.getColumnIndex(LANGUAGE)));
         userModel.setIsFavorite(cursor.getInt(cursor.getColumnIndex(IS_FAVOURITE)));
        return userModel;
    }
    public UserModel getUserById(int id){
        SQLiteDatabase db=getReadableDatabase();
        UserModel userModel=new UserModel();
        String query="SELECT * FROM "+ TABLE_NAME +" WHERE "+ USER_ID +" =? ";
        Cursor cursor=db.rawQuery(query,new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        userModel = getCreatedModelUsingCursor(cursor);
        cursor.close();
        db.close();
        return userModel;
    }
    public ArrayList<UserModel> getUserListByGender(int gender) {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                "SELECT " +
                        " UserId," +
                        " TblMst_User.Name as Name," +
                        " FatherName," +
                        " SurName," +
                        " Gender," +
                        " Hobbies," +
                        " Dob," +
                        " Phone_Number," +
                        " IsFavourite," +
                        " Tbl_Language.Language_Id," +
                        " Tbl_City.City_Id," +
                        " Tbl_Language.Language_Name as Language," +
                        " Tbl_City.CityName as City " +

                        "FROM " +
                        " TblMst_User " +
                        " INNER JOIN Tbl_Language ON TblMst_User.Language_Id = Tbl_Language.Language_Id" +
                        " INNER JOIN Tbl_City ON TblMst_User.City_Id = Tbl_City.City_Id" +
                        " WHERE " +
                        " Gender = ?";

        Cursor cursor=db.rawQuery(query,new String[]{String.valueOf(gender)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        Log.d("CursorSize",""+cursor.getColumnCount());
        cursor.close();
        db.close();
        return list;
    }
    public ArrayList<UserModel> getFavoriteUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query =
                "SELECT " +
                        " UserId," +
                        " TblMst_User.Name as Name," +
                        " FatherName," +
                        " SurName," +
                        " Gender," +
                        " Hobbies," +
                        " Dob," +
                        " Phone_Number," +
                        " IsFavourite," +
                        " Tbl_Language.Language_Id," +
                        " Tbl_City.City_Id," +
                        " Tbl_Language.Language_Name as Language," +
                        " Tbl_City.CityName as City " +

                        "FROM " +
                        " TblMst_User " +
                        " INNER JOIN Tbl_Language ON TblMst_User.Language_Id = Tbl_Language.Language_Id" +
                        " INNER JOIN Tbl_City ON TblMst_User.City_Id = Tbl_City.City_Id" +
                        " WHERE " +
                        " IsFavourite = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(1)});
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            list.add(getCreatedModelUsingCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
    public long insertUser(String name,String fathername,String surname,int gender,String hobbies,String dob,
                           String phonenumber,int languageid,int cityid,int isfavourite)
    {
        SQLiteDatabase db=getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(NAME,name);
        cv.put(FATHER_NAME,fathername);
        cv.put(SURNAME,surname);
        cv.put(GENDER,gender);
        cv.put(HOBBIES,hobbies);
        cv.put(DOB,dob);
        cv.put(PHONE_NUMBER,phonenumber);
        cv.put(LANGUAGE_ID,languageid);
        cv.put(CITY_ID,cityid);
        cv.put(IS_FAVOURITE,isfavourite);
       long lastinsertedid= db.insert(TABLE_NAME,null,cv);
        db.close();
        return lastinsertedid;
    }
    public int updateUserbyid(int userId,String name,String fathername,String surname,int gender,String hobbies,String dob,
                           String phonenumber,int languageid,int cityid,int isfavourite)
    {
        SQLiteDatabase db=getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(NAME,name);
        cv.put(FATHER_NAME,fathername);
        cv.put(SURNAME,surname);
        cv.put(GENDER,gender);
        cv.put(HOBBIES,hobbies);
        cv.put(DOB,dob);
        cv.put(PHONE_NUMBER,phonenumber);
        cv.put(LANGUAGE_ID,languageid);
        cv.put(CITY_ID,cityid);
        cv.put(IS_FAVOURITE,isfavourite);
        int lastuptedid= db.update(TABLE_NAME,cv,USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return lastuptedid;
    }
    public int deleteUserById(int userId) {
        SQLiteDatabase db = getWritableDatabase();
        int deletedUserID = db.delete(TABLE_NAME, USER_ID + " = ? ", new String[]{String.valueOf(userId)});
        db.close();
        return deletedUserID;
    }
    public int deleteAll()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "1", null);
    }
    public int updateFavoriteStatus(int isFavorite, int userId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(IS_FAVOURITE, isFavorite);
        int lastUpdatedId = db.update(TABLE_NAME, cv, USER_ID + " = ?", new String[]{String.valueOf(userId)});
        db.close();
        return lastUpdatedId;
    }
}
