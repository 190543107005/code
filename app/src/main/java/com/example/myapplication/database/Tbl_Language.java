package com.example.myapplication.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapplication.model.LanguageModel;

import java.util.ArrayList;

public class Tbl_Language extends MyDatabase {
    public static final String TABLE_NAME = "Tbl_Language";
    public static final String LANGUAGE_ID = "Language_Id";
    public static final String NAME = "Language_Name";
    public Tbl_Language(Context context) {
        super(context);
    }

    public ArrayList<LanguageModel> getLanguages() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguageModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        LanguageModel languageModel1 = new LanguageModel();
        languageModel1.setLanguage_Name("Select One");
        list.add(0, languageModel1);
        for (int i = 0; i < cursor.getCount(); i++) {
            LanguageModel languageModel = new LanguageModel();
            languageModel.setLanguage_Id(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            languageModel.setLanguage_Name(cursor.getString(cursor.getColumnIndex(NAME)));
            list.add(languageModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
}
