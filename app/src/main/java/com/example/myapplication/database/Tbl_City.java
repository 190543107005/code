package com.example.myapplication.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapplication.model.CityModel;

import java.util.ArrayList;

public class Tbl_City extends MyDatabase {

    public static final String TABLE_NAME = "Tbl_City";
    public static final String CITY_ID = "City_Id";
    public static final String NAME = "CityName";

    public Tbl_City(Context context) {
        super(context);
    }

    public ArrayList<CityModel> getCityList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<CityModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        CityModel cityModel1 = new CityModel();
        cityModel1.setCityName("Select One");
        list.add(0, cityModel1);
        for (int i = 0; i < cursor.getCount(); i++) {
            CityModel cityModel = new CityModel();
            cityModel.setCity_Id(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            cityModel.setCityName(cursor.getString(cursor.getColumnIndex(NAME)));
            list.add(cityModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public CityModel getCityById(int id) {
        SQLiteDatabase db = getReadableDatabase();
        CityModel model = new CityModel();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + CITY_ID + " = ?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        model.setCityName(cursor.getString(cursor.getColumnIndex(NAME)));
        model.setCity_Id(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        cursor.close();
        db.close();
        return model;
    }
}
