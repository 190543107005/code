package com.example.myapplication.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("search")
    Call<ResponseBody> getTopRatedMovies(@Query("name") String name);

    @GET("movie/{id}")
    Call<ResponseBody> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
}
