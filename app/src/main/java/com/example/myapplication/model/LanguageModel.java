package com.example.myapplication.model;

import java.io.Serializable;

public class LanguageModel implements Serializable {
    int Language_Id;
    String Language_Name;

    public int getLanguage_Id() {
        return Language_Id;
    }

    public void setLanguage_Id(int language_Id) {
        Language_Id = language_Id;
    }

    public String getLanguage_Name() {
        return Language_Name;
    }

    public void setLanguage_Name(String language_Name) {
        Language_Name = language_Name;
    }

    @Override
    public String toString() {
        return "LanguageModel{" +
                "Language_Id=" + Language_Id +
                ", Language_Name='" + Language_Name + '\'' +
                '}';
    }
}
