package com.example.myapplication.model;

import java.io.Serializable;
import java.util.ArrayList;

public class CollageList implements Serializable {

    String name;
    ArrayList<String> domains = new ArrayList<>();
    String alpha_two_code;
    String country;
    ArrayList<String> web_pages = new ArrayList<>();
    double state_province;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getDomains() {
        return domains;
    }

    public void setDomains(ArrayList<String> domains) {
        this.domains = domains;
    }

    public String getAlpha_two_code() {
        return alpha_two_code;
    }

    public void setAlpha_two_code(String alpha_two_code) {
        this.alpha_two_code = alpha_two_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public ArrayList<String> getWeb_pages() {
        return web_pages;
    }

    public void setWeb_pages(ArrayList<String> web_pages) {
        this.web_pages = web_pages;
    }

    public double getState_province() {
        return state_province;
    }

    public void setState_province(double state_province) {
        this.state_province = state_province;
    }

    @Override
    public String toString() {
        return "CollageList{" +
                "name='" + name + '\'' +
                ", domains=" + domains +
                ", alpha_two_code='" + alpha_two_code + '\'' +
                ", country='" + country + '\'' +
                ", web_pages=" + web_pages +
                ", state_province=" + state_province +
                '}';
    }
}
