package com.example.myapplication.model;

import java.io.Serializable;

public class UserModel implements Serializable {
    int UserId;
    String Name;
    String FatherName;
    String SurName;
    int Gender;
    String Dob;
    String Phone_Number;
    String Hobbies;
    int Language_Id;
    int City_Id;
    String City;
    String Language;
    int IsFavorite;

    public int getIsFavorite() {
        return IsFavorite;
    }

    public void setIsFavorite(int isFavorite) {
        IsFavorite = isFavorite;
    }

    public String getCity() {
        return City;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public void setCity(String city) {
        City = city;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }



    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public String getPhone_Number() {
        return Phone_Number;
    }

    public void setPhone_Number(String phone_Number) {
        Phone_Number = phone_Number;
    }
    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public int getLanguage_Id() {
        return Language_Id;
    }

    public void setLanguage_Id(int language_Id) {
        Language_Id = language_Id;
    }

    public int getCity_Id() {
        return City_Id;
    }

    public void setCity_Id(int city_Id) {
        City_Id = city_Id;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "UserId=" + UserId +
                ", Name='" + Name + '\'' +
                ", FatherName='" + FatherName + '\'' +
                ", SurName='" + SurName + '\'' +
                ", Gender=" + Gender +
                ", Dob='" + Dob + '\'' +
                ", Phone_Number='" + Phone_Number + '\'' +
                ", Hobbies='" + Hobbies + '\'' +
                ", Language_Id=" + Language_Id +
                ", City_Id=" + City_Id +
                ", City='" + City + '\'' +
                ", Language='" + Language + '\'' +
                ", IsFavorite=" + IsFavorite +
                '}';
    }
}


