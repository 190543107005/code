package com.example.myapplication.model;

import java.io.Serializable;

public class CityModel implements Serializable {
    int City_Id;
    String CityName;

    public int getCity_Id() {
        return City_Id;
    }

    public void setCity_Id(int city_Id) {
        City_Id = city_Id;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    @Override
    public String toString() {
        return "CityMoel{" +
                "City_Id=" + City_Id +
                ", CityName='" + CityName + '\'' +
                '}';
    }
}
