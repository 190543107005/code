package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.CollageList;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClgListAdapter extends RecyclerView.Adapter<ClgListAdapter.ClgHolder> {
    Context context;
    ArrayList<CollageList> clgList;

    public ClgListAdapter(Context context, ArrayList<CollageList> clgList) {
        this.context = context;
        this.clgList = clgList;
    }

    @NonNull
    @Override

    public ClgHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ClgHolder(LayoutInflater.from(context).inflate(R.layout.view_row_clg_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ClgHolder holder, int position) {
        holder.tvData.setText("University : " + clgList.get(position).getName());
        holder.tvlist.setText("Get Alpha To Code : " + clgList.get(position).getAlpha_two_code()+"\n"
              + "Country : " +clgList.get(position).getCountry()+"\n"
               + "State_Provience : " +clgList.get(position).getState_province());
                holder.tvlink.setText("Web_Pages : " +clgList.get(position).getWeb_pages()+"\n"
                        + "Domains"+ clgList.get(position).getDomains());
    }

    @Override
    public int getItemCount() {
        return clgList.size();
    }

    class ClgHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvData)
        TextView tvData;
        @BindView(R.id.tvlist)
        TextView tvlist;
        @BindView(R.id.tvlink)
        TextView tvlink;

        public ClgHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
