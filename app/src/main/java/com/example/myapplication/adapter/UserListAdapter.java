package com.example.myapplication.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserHolder> {

    Context context;
    ArrayList<UserModel> userList;
    OnDeleteClickListner  onDeleteClick;
    public UserListAdapter(Context context, ArrayList<UserModel> userList,OnDeleteClickListner onDeleteClick) {
        this.context = context;
        this.userList = userList;
        this.onDeleteClick=onDeleteClick;
    }

    public interface OnDeleteClickListner {
        void OnClick(int position);

        void OnItemView(int position);

        void onFavouriteClick(int position);
    }

    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.layout_view_row_user_list, null));
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        holder.tvFullName.setText(userList.get(position).getName() + " " + userList.get(position).getSurName());
        holder.tvLanguage.setText(userList.get(position).getLanguage() + " | " + userList.get(position).getCity());
        holder.tvDob.setText(userList.get(position).getDob());
        holder.ivFavouriteUser.setImageResource(userList.get(position).getIsFavorite() == 0 ?R.drawable.baseline_star_outline_black_24:R.drawable.baseline_star_rate_black_24);
        holder.ivDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onDeleteClick!=null)
                {
                    onDeleteClick.OnClick(position);
                }
            }
        });
       holder.itemView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               if(onDeleteClick!=null)
               {
                   onDeleteClick.OnItemView(position);
               }
           }
       });
       holder.ivFavouriteUser.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
            if(onDeleteClick!=null) {
                onDeleteClick.onFavouriteClick(position);
            }
           }
       });
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvFullName)
        TextView tvFullName;
        @BindView(R.id.tvLanguage)
        TextView tvLanguage;
        @BindView(R.id.tvDob)
        TextView tvDob;
        @BindView(R.id.ivDeleteUser)
        ImageView ivDeleteUser;
        @BindView(R.id.ivFavouriteUser)
        ImageView ivFavouriteUser;



        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
