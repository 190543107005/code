package com.example.myapplication.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myapplication.fragment.FragmentUserList;
import com.example.myapplication.util.Constant;

public class GenderViewPagerAdapter extends FragmentStatePagerAdapter {

    private String tabTitles[] = new String[]{"Male", "Female"};
    private Context context;

    public GenderViewPagerAdapter(@NonNull FragmentManager fm, int behavior, Context context) {
        super(fm, behavior);
        this.context = context;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return FragmentUserList.getInstance(Constant.Male);
        } else {
            return FragmentUserList.getInstance(Constant.Female);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @NonNull
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
