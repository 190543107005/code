package com.example.myapplication;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.example.myapplication.adapter.GenderViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.fragment.app.FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;

public class ActivituUserListByGender extends BaseActivity {
    @BindView(R.id.Genders)
    TabLayout Genders;
    @BindView(R.id.vpUserList)
    ViewPager vpUserList;
    GenderViewPagerAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.genderwise_list);
        ButterKnife.bind(this);
        setUpActionBar(getString(R.string.Text_UserList),true);
        setUpViewPagerAdapter();
    }


    void setUpViewPagerAdapter() {
        adapter = new GenderViewPagerAdapter(getSupportFragmentManager(),BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, this);
        vpUserList.setAdapter(adapter);
        Genders.setupWithViewPager(vpUserList);
    }

}
