package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.adapter.UserListAdapter;
import com.example.myapplication.database.TblMst_User;
import com.example.myapplication.model.UserModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavouriteUserActivity extends BaseActivity {
    @BindView(R.id.rcvUserList)
    RecyclerView rcvUserList;
    ArrayList<UserModel> userList = new ArrayList<>();
    UserListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        ButterKnife.bind(this);
        setUpActionBar("Favourite User",true);
        setAdapter();
    }
    void setAdapter(){
        rcvUserList.setLayoutManager(new GridLayoutManager(this,1));
        userList.addAll(new TblMst_User(this).getFavoriteUserList());
        adapter= new UserListAdapter(this,userList,null);
        rcvUserList.setAdapter(adapter);
    }
    void sendFavouriteChange(){
        Intent intent = new Intent();
    }
}
